const mongoose = require('mongoose');
const { Schema } = mongoose;

const ClientSchema =  new Schema({
    name: { type: String, required: true },
    id_n: { type: String, required: true },
    age: { type: Number, required: true },
    gender: { type: String, required: true },
    birthdate: { type: String, required: true }, 
    address: { type: String, required: true }
});

module.exports = mongoose.model('Client', ClientSchema);