const mongoose = require('mongoose');

const URI = 'mongodb://localhost/market';

mongoose.connect(URI, {useNewUrlParser: true})
    .then (db => console.log('Database connected'))
    .catch(err => console.error(err));

module.exports = mongoose;