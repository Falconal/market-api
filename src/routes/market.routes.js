const express = require('express');
const router = express.Router();

//To do: change post so it actually saves by using client.save; also add other requests

//Required models
const Client = require('../models/client');
const Product = require('../models/product');
const Staff = require('../models/staff');

//Client requests

router.get('/clients', async (req, res) => {
    const clients = await Client.find();
    res.json(clients);
});

router.get('/clients/:id', async (req, res) => {
    const client = await Client.findById(req.params.id);
    res.json(client);
});

router.post('/clients', async (req, res) => {
    const { name, id_n, age, gender, birthdate, address } = req.body;
    const client = new Client({name, id_n, age, gender, birthdate, address});
    await client.save();
    res.json({
        status: 'Received Client data',
        client_id: client.id
    });
});

router.put('/clients/:id', async (req, res) => {
    const { name, id_n, age, gender, birthdate, address } = req.body;
    const newClient = {name, id_n, age, gender, birthdate, address};
    await Client.findByIdAndUpdate(req.params.id, newClient);
    res.json({status: 'Client data Updated'});
});

router.delete('/clients/:id', async (req, res) => {
    await Client.findByIdAndDelete(req.params.id);
    res.json({status: 'Client data Deleted'});
});


//Product requests

router.get('/products', async (req, res) => {
    const products = await Product.find();
    res.json(products);
});

router.get('/products/:id', async (req, res) => {
    const product = await Product.findById(req.params.id);
    res.json(product);
});

router.post('/products', async (req, res) => {
    const { name, quantity, category, price } = req.body;
    const product = new Product({name, quantity, category, price});
    await product.save();
    res.json({
        status: 'Received Product data',
        product_id: product.id 
    });
});

router.put('/products/:id', async (req, res) => {
    const { name, quantity, category, price } = req.body;
    const newProduct = {name, quantity, category, price};
    await Product.findByIdAndUpdate(req.params.id, newProduct);
    res.json({status: 'Product data Updated'});
});

router.delete('/products/:id', async (req, res) => {
    await Product.findByIdAndDelete(req.params.id);
    res.json({status: 'Product data Deleted'});
});


//Staff requests

router.get('/staff', async (req, res) => {
    const staff = await Staff.find();
    res.json(staff);
});

router.get('/staff/:id', async (req, res) => {
    const member = await Staff.findById(req.params.id);
    res.json(member);
});

router.post('/staff', async (req, res) => {
    const { name, id_n, age, gender, birthdate, address, user, password, email } = req.body;
    const member = new Staff({name, id_n, age, gender, birthdate, address, user, password, email});
    await member.save();
    res.json({
        status: 'Received Staff Member data',
        member_id: member.id
    });
});

router.put('/staff/:id', async (req, res) => {
    const { name, id_n, age, gender, birthdate, address, user, password, email } = req.body;
    const newMember = {name, id_n, age, gender, birthdate, address, user, password, email};
    await Staff.findByIdAndUpdate(req.params.id, newMember);
    res.json({status: 'Staff Member data Updated'});
});

router.delete('/staff/:id', async (req, res) => {
    await Staff.findByIdAndDelete(req.params.id);
    res.json({status: 'Staff Member data Deleted'});
});

module.exports = router;